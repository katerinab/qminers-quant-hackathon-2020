import math

import numpy as np
import pandas as pd
from time import time
from pathlib import Path
from model import PARAMS

INIT_BANKROLL = 1000

if PARAMS['autoplot']:
    from plot_results import plot

if PARAMS['print memory usage']:
    import os
    import psutil


class Environment:
    def __init__(self, dataset, interactor, init_bankroll=1000, min_bet=5, max_bet=100):
        dataset['BetH'] = 0.
        dataset['BetD'] = 0.
        dataset['BetA'] = 0.
        self.dataset = dataset
        self.interactor = interactor
        self.bankroll = init_bankroll
        self.min_bet = min_bet
        self.max_bet = max_bet
        self.last_seen = pd.to_datetime('1900-01-01')
        self.bet_cols = ['BetH', 'BetD', 'BetA']
        self.odds_cols = ['OddsH', 'OddsD', 'OddsA']
        self.score_cols = ['HSC', 'ASC']
        self.res_cols = ['H', 'D', 'A']
        self.label_cols = self.score_cols + self.res_cols
        self.results_fn = None
        self.cur_idx = None

    def get_incremental_data(self, date):
        inc = self.dataset.loc[(self.dataset.Date > self.last_seen) & (self.dataset.Date < date)]
        self.last_seen = inc.Date.max() if not inc.empty else self.last_seen
        return inc

    def get_opps(self, date):
        opps = self.dataset[(self.dataset.Open <= date) & (self.dataset.Date >= date)]
        # opps = self.dataset[self.dataset.Date >= date]
        opps = opps[opps[self.odds_cols].sum(axis=1) > 0]  # todo change back
        return opps.drop(self.label_cols, axis=1)

    def save_results(self, days, total, i, kwargs=None):
        if kwargs is None:
            kwargs = {}

        if self.results_fn is None:
            results_path = Path.cwd().parent / 'results'
            results_path.mkdir(parents=True, exist_ok=True)
            hp_path = Path.cwd().parent / 'hyperparams'
            hp_path.mkdir(parents=True, exist_ok=True)
            last_idx = max((int(file.stem) for file in results_path.glob('*.txt')), default=0)
            self.cur_idx = last_idx + 1
            self.results_fn = f'../results/{self.cur_idx:03}'
            self.hyperparams_path = f'../hyperparams/{self.cur_idx:03}.txt'
            with open(self.hyperparams_path, 'w') as f:
                f.write('PARAMS = {\n')
                for key, value in PARAMS.items():
                    f.write(f'\t\'{key}\': {value},\n')
                f.write('}\n')
            for k, x in kwargs:
                self.results_fn += f'_{k}-{x}'
            self.results_fn += '.txt'

        print(f'Saving results to {self.results_fn}.')
        with open(self.results_fn, 'a') as f:
            # f.write(f'{kwargs}\n')
            for day, t in zip(days, total):
                f.write(f'{day.date()} {t:.3f}\n')
        if PARAMS['autoplot'] and i > 20:
            plot(PLOT_LATEST=False, fn=f'{self.cur_idx:03}.txt')

    def run(self, start=None, end=None):
        start_time = time()

        start = start if start is not None else self.dataset.Open.min()
        end = end if end is not None else self.dataset.Date.max()

        days = []
        total_array = []
        # times = []
        current_year = []
        print(f"Start: {start}, End: {end}")
        for i, date in enumerate(pd.date_range(start, end)):

            opps = self.get_opps(date)
            if opps.empty:
                continue

            inc = self.get_incremental_data(date)
            
            placed = opps[self.bet_cols].sum().sum()

            self.bankroll += self.evaluate_bets(inc)

            days.append(date)
            total_array.append(self.bankroll + placed)
            if i % PARAMS['rewrite results every x days (and plot)'] == 0:
                self.save_results(days, total_array, i, kwargs=None)
                days = []
                total_array = []

            summary = self.generate_summary(date)
            print(f'({time() - start_time:7.2f} s) | {date:%Y-%m-%d}: available: {self.bankroll:.2f}, invested '
                  f'{placed:.2f}, total {self.bankroll+placed:.2f}'
                  f', SCORE {math.log(float(self.bankroll + placed)/INIT_BANKROLL, 2):.3f}')

            bets = self.get_bets(summary, inc, opps)

            validated_bets = self.validate_bets(bets, opps)

            self.place_bets(validated_bets)

            if PARAMS['print memory usage']:
                process = psutil.Process(os.getpid())
                print(f'MEMORY: {process.memory_info().rss // 2**20:} MB')

        self.bankroll += self.evaluate_bets(self.get_incremental_data(end + pd.to_timedelta(1, 'days')))

        total_array.append(self.bankroll)
        self.save_results(days, total_array, i + 1, kwargs=None)

        with open(self.hyperparams_path, 'a') as f:
            f.write(f'\n({time() - start_time:7.2f} s) s\tfinal bankroll = {self.bankroll}\n')
        # total_array.append(self.bankroll)
        # one day missing
        # self.save_results(days, total_array, kwargs=None)

        if hasattr(self.interactor, 'writeln'):
            self.send_updates(pd.DataFrame(), pd.DataFrame(), pd.DataFrame())

        return self.dataset

    def validate_bets(self, bets, opps):
        #print("Validating bets")
        if bets is not None:
            rows = bets.index.intersection(opps.index)
            cols = bets.columns.intersection(self.bet_cols)
            validated_bets = bets.loc[rows, cols]  # allow bets only on the send opportunities
            validated_bets[validated_bets < self.min_bet] = 0.  # reject bets lower than min_bet
            validated_bets[validated_bets > self.max_bet] = 0.  # reject bets higher than max_bet
            if validated_bets.sum().sum() > self.bankroll:  # reject bets if there are no sufficient funds left
                validated_bets.loc[:, :] = 0.
            return validated_bets

    def place_bets(self, bets):
        #print("Placing bets")
        if bets is not None:
            self.dataset.loc[bets.index, self.bet_cols] = self.dataset.loc[bets.index, self.bet_cols].add(bets, fill_value=0)
            self.bankroll -= bets.values.sum()

    def evaluate_bets(self, inc):
        if inc.empty:
            return 0
        b = inc[self.bet_cols].values
        o = inc[self.odds_cols].values
        r = inc[self.res_cols].values
        winnings = (b * r * o).sum(axis=1)
        return winnings.sum()

    def generate_summary(self, date):
        summary = {
            'Bankroll': self.bankroll,
            'Date': date,
            'Min_bet': self.min_bet,
            'Max_bet': self.max_bet,
        }
        return pd.Series(summary).to_frame().T

    def get_bets(self, summary: pd.DataFrame, inc: pd.DataFrame, opps: pd.DataFrame) -> pd.DataFrame:
        return self.interactor.place_bets(opps, summary, inc)

