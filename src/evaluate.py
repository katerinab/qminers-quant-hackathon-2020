import pandas as pd

import sys
sys.path.append(".")

from model import Model
# from pi_model import Model

from environment import Environment
from environment import INIT_BANKROLL

dataset = pd.read_csv('../data/training_data_finals.csv', parse_dates=['Date', 'Open'])
model = Model()

env = Environment(dataset, model, init_bankroll=INIT_BANKROLL, min_bet=5., max_bet=100.)
# evaluation = env.run(start=pd.to_datetime('2005-07-01'))  # 8000 matches
# evaluation = env.run(start=pd.to_datetime('2000-11-26'))  # 800 matches
# evaluation = env.run(start=pd.to_datetime('2000-04-19'))  # 100 matches
#evaluation = env.run(start=pd.to_datetime('2003-09-10'))  # finals
evaluation = env.run(start=pd.to_datetime('2011-07-01'))  # last two seasons



print(f'Final bankroll: {env.bankroll:.2f}')

