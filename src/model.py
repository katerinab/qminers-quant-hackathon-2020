import math
from enum import Enum
from typing import Tuple

import numpy as np
import pandas as pd
from collections import defaultdict
from sklearn.ensemble import GradientBoostingClassifier
from xgboost import XGBClassifier
import sys
# import stats as st
from time import time

'''
Qminers Hackathon 2020 by team Nanuk

The task is to develop an online betting strategy based on historical data about football matches and presented betting
opportunities. The data consist of date; UIDs of match, teams, league; bookmaker rates; and final score.
Training data also include one-hot encoding of win, draw, loss.

Our algorithm is largely based on paper by Hubáček, Ondřej & Sourek, Gustav & Železný, Filip. (2019).
Learning to predict soccer results from relational data with gradient boosted trees.
Machine Learning. 108. 10.1007/s10994-018-5704-6. 

The main idea is to 'hand-craft' additional features of the match.
Features include:   1) Historical strength of the team - win/draw home/away percentage, mean/std of scored goals, ...
                                                       - calculated from last two and current season
                    2) Current form of the team - similar as above but calculated for last five matches
                    3) League features - gives context for 1) and 2), i.e. calculates avg/std goals, etc.
                    4) Pi-ratings - similar to ELO rating for each team

The final feature vector for one match has a length of ~70.
We use Gradient Boosting Classifier by scikit to predict probabilities of win/draw/loss.
In case that our prediction is higher by x (~0.02) from the bookmaker, we make a bet.

Our algorithm gradually computes feature vectors from the received data, saves it and retrains the model every x months.
It can happen, that we do not have enough data to calculate some of the features. Then the match is discarded from
learning.

As the vectors are very long and not that accurate with not enough info about team, it can happen, that GBC gives biased 
/ unstable predictions - especially in the beginning of the run.

What we also tried - 1) modify parameters for GBC to handle bias/variance better + try xgboost
                     2) give higher weight to more recent feature vectors
                     3) different betting strategies
We did not perform any sophisticated search for hyper parameters, so this is the area we would focus on if we had more
time.

As the code is work in progress, it is not perfectly documented, clean or optimized for speed.
If there should be some uncertainties, don't hesitate to contact us for clarification.
'''

class BettingStrategy(Enum):
    AGGRESSIVE, LESS_AGGRESSIVE, COMPLEX = range(3)

class SampleWeightStrategy(Enum):
    NONE, BETA, LINEAR = range(3)

VERBOSE = False

PARAMS = {
    'use_scikit': True,  # False for xgb
    'pi_rating_lambda': 0.06,
    'pi_rating_gamma': 0.5,
    'pi_rating_log_base': 10,
    'pi_rating_C': 3,
    'GBC_max_depth': 4,
    'GBC_subsample': 0.8,
    'GBC_n_estimators': 40,
    'GBC_xgb_col_sample_bytree': 0.25,
    'GBC_xgb_min_child_weight': 5,
    'GBC_sample_weight': SampleWeightStrategy.NONE,
    'GBC_beta_distribution_params': {'alpha': 5, 'beta': 1},
    'current_form_feature_from_last_n': 5,
    'num_league_matches_to_ignore': 30,
    'num_team_matches_to_ignore': 8,
    'points_for_win': 3,
    'points_for_loss': 1,
    'minimal_league_table_size': 10,
    'num_top_and_bottom_league_table_teams_for_stats': 5,
    'train_every_n_days': 366,
    'bet_ratio_small': 6,
    'bet_ratio_large': 2,
    'bet_ratio_resources_bound': 3000,
    'bet_aggressive_probability_bound': 0.01,
    'bet_less_aggressive_probability_bounds': (0.01, 0.005),
    'bet_strategy_type': BettingStrategy.LESS_AGGRESSIVE,
    'autoplot': True,
    'rewrite results every x days (and plot)': 30,
    'print memory usage': False,
    'league_features_fast': True
}


def naive_variance(sum_sq, sum, n):
    return (sum_sq - ((sum * sum) / n)) / (n-1)


def get_lt_tag(leagueID, teamID):
    return leagueID + "_" + str(teamID)


class PiRatingCalculator:
    def __init__(self):
        self.LEARNING_RATE_LAMBDA = PARAMS['pi_rating_lambda']
        self.LEARNING_RATE_GAMMA = PARAMS['pi_rating_gamma']
        self.LOG_BASE = PARAMS['pi_rating_log_base']
        self.C = PARAMS['pi_rating_C']

    def get_expected_goal_diff(self, home_pi_rating, away_pi_rating):
        # expected goal difference against average team
        a = np.array([home_pi_rating, away_pi_rating])
        gd = np.sign(a) * (np.power(self.LOG_BASE, np.abs(a) / self.C) - 1)
        return gd[0] - gd[1]

    def weighted_error(self, goal_diff, expected_goal_diff):
        # returns weighted home error, weighted away error
        error = np.abs(goal_diff - expected_goal_diff)
        # c*logB(1+e)
        we = (1 if expected_goal_diff < goal_diff else -1) * self.C * np.log(1 + error) / np.log(self.LOG_BASE)
        return we, -we

    def get_updated_pi_ratings(self, home_pi_H, home_pi_A, away_pi_H, away_pi_A, goal_diff):
        weh, wea = self.weighted_error(goal_diff,
                                       self.get_expected_goal_diff(home_pi_H, away_pi_A)
                                       # doesn't have to be computed again
                                       )
        new_home_pi_H = home_pi_H + weh * self.LEARNING_RATE_LAMBDA
        new_home_pi_A = home_pi_A + (new_home_pi_H - home_pi_H) * self.LEARNING_RATE_GAMMA
        new_away_pi_A = away_pi_A + wea * self.LEARNING_RATE_LAMBDA
        new_away_pi_H = away_pi_H + (new_away_pi_A - away_pi_A) * self.LEARNING_RATE_GAMMA
        return new_home_pi_H, new_home_pi_A, new_away_pi_H, new_away_pi_A


class Model:

    def __init__(self):
        # feature dicts indexed by team id, league id
        self.team_features = defaultdict(dict)
        # {team1: {avgGoals: 0, maxGoals: 1}, team2: {avgGoals: 0, maxGoals: 1}}
        self.league_features = defaultdict(dict)
        if PARAMS['use_scikit']:
            self.model = GradientBoostingClassifier(max_depth=PARAMS['GBC_max_depth'],
                                                    subsample=PARAMS['GBC_subsample'],
                                                    n_estimators=PARAMS['GBC_n_estimators'])
        else:
            self.model = XGBClassifier(max_depth=PARAMS['GBC_max_depth'],
                                       subsample=PARAMS['GBC_subsample'],
                                       n_estimators=PARAMS['GBC_n_estimators'],
                                       min_child_weight=PARAMS['GBC_xgb_min_child_weight'],
                                       colsample_bytree=PARAMS['GBC_xgb_col_sample_bytree'])
        self.history_in_features = []
        self.historical_results = []
        self.n_team_features = 12 + 6 + 1
        self.n_league_features = 8
        self.n_pi_features = 3
        self.n_match_importance_features = 4 * PARAMS['num_top_and_bottom_league_table_teams_for_stats'] + 1
        self.last_trained_date = None
        self.train_every_n_days = PARAMS['train_every_n_days']
        # self.stats = st.Stats()
        self.start_time = time()
        self.pi = PiRatingCalculator()
        self.match2feature_vector = {}
        self.current_form_from_last_n = PARAMS['current_form_feature_from_last_n']
        self.ctr = 0
        self.calc_league_feature = self.calc_league_feature_fast if PARAMS['league_features_fast'] else self.calc_league_feature_slow

        # print hyperparams
        if VERBOSE:
            for i in PARAMS.items():
                print(i)

    def update(self, new_results: pd.DataFrame):
        # update team features
        for j in ['HID', 'AID']:
            for home_team_ID in new_results[j].unique():
                home_matches = new_results[new_results[j] == home_team_ID]
                for leagueID in home_matches['LID'].unique():
                    self.update_team_history(home_team_ID, leagueID, home_matches, j[0])

        for team_ID in new_results['HID'].append(new_results['AID']).unique():
            matches = new_results.loc[(new_results['HID'] == team_ID) | (new_results['AID'] == team_ID)]
            for leagueID in matches['LID'].unique():
                self.update_team_history(team_ID, leagueID, matches, 'F')

        for league_ID in new_results['LID'].unique():
            matches = new_results.loc[(new_results['LID'] == league_ID)]
            history_label = f'L_HISTORY'
            if history_label not in self.league_features[league_ID].keys():
                self.league_features[league_ID][history_label] = matches
            else:
                self.league_features[league_ID][history_label] = self.league_features[league_ID][history_label].append(matches)

    def get_goals_scored_avg_in_matches(self, goals_scored: np.ndarray):
       return goals_scored.mean()

    def get_goals_conceded_avg_in_matches(self, goals_conceded: np.ndarray):
        return goals_conceded.mean()

    def get_goals_scored_stddev_in_matches(self, goals_scored: np.ndarray):
        stddev = goals_scored.std()
        return 0.0 if np.isnan(stddev) else stddev

    def get_goals_conceded_stddev_in_matches(self, goals_conceded: np.ndarray):
        stddev = goals_conceded.std()
        return 0.0 if np.isnan(stddev) else stddev

    def get_scored_conceded_goals_in_matches(self, teamID, matches: pd.DataFrame) -> Tuple[np.ndarray, np.ndarray]:
        matches_where_home = matches[matches['HID'] == teamID]
        matches_where_away = matches[matches['AID'] == teamID]
        goals_scored = np.concatenate((matches_where_home['HSC'].to_numpy(), matches_where_away['ASC'].to_numpy()))
        goals_conceded = np.concatenate((matches_where_home['ASC'].to_numpy(), matches_where_away['HSC'].to_numpy()))
        return goals_scored, goals_conceded

    def get_scored_goals_in_matches(self, teamID: int, matches: pd.DataFrame) -> np.ndarray:
        # TODO: remove?
        goals_when_home = matches[matches['HID'] == teamID]['HSC'].to_numpy()
        goals_when_away = matches[matches['AID'] == teamID]['ASC'].to_numpy()
        return np.concatenate((goals_when_home, goals_when_away))

    def get_conceded_goals_in_matches(self, teamID: int, matches: pd.DataFrame) -> np.ndarray:
        # TODO: remove?
        goals_when_home = matches[matches['HID'] == teamID]['ASC'].to_numpy()
        goals_when_away = matches[matches['AID'] == teamID]['HSC'].to_numpy()
        return np.concatenate((goals_when_home, goals_when_away))

    def get_days_since_last_match_from_matches(self, date, matches):
        return float(int((date - matches.tail(n=1)['Date']).dt.days))  # maybe float straight away? Not sure rn

    def update_current_form(self, Date, HID, AID, LID, HSC, ASC, H, D, A):  # todo remove this comment todo remove this comment todo remove this comment todo remove this comment
        self.update_team_current_form(Date, HID, LID, HSC, ASC, H, D)
        self.update_team_current_form(Date, AID, LID, ASC, HSC, A, D)

    def update_team_current_form(self, date, teamID, LID, scored_goals, conceded_goals, won, draw):
        team_tag = get_lt_tag(LID, teamID)
        if 'current_form' not in self.team_features[team_tag]:
            self.team_features[team_tag]['current_form'] = {'observed': 0,
                                                            'cur_idx': 0,
                                                            'scored_goals': np.zeros(shape=(self.current_form_from_last_n,), dtype=int),
                                                            'conceded_goals': np.zeros(shape=(self.current_form_from_last_n,), dtype=int),
                                                            'wins': np.zeros(shape=(self.current_form_from_last_n,), dtype=int),
                                                            'draws': np.zeros(shape=(self.current_form_from_last_n,), dtype=int),
                                                            'last_date': None}
        current_form = self.team_features[team_tag]['current_form']
        current_form['observed'] = min(current_form['observed']+1, self.current_form_from_last_n)
        cur_idx = current_form['cur_idx']
        current_form['scored_goals'][cur_idx] = scored_goals
        current_form['conceded_goals'][cur_idx] = conceded_goals
        current_form['wins'][cur_idx] = won
        current_form['draws'][cur_idx] = draw
        current_form['cur_idx'] = (cur_idx + 1) % self.current_form_from_last_n
        current_form['last_date'] = date

    def get_current_form(self, teamID, leagueID, date):
        dummy_vec = [0.0 for _ in range(7)]
        team_tag = get_lt_tag(leagueID, teamID)
        if 'current_form' not in self.team_features[team_tag]:
            return dummy_vec, False
        current_form = self.team_features[team_tag]['current_form']
        if current_form['observed'] < self.current_form_from_last_n:
            return dummy_vec, False

        goals_scored, goals_conceded = current_form['scored_goals'], current_form['conceded_goals']

        winPct = current_form['wins'].mean()
        drawPct = current_form['draws'].mean()
        goalsScoredAvg = self.get_goals_scored_avg_in_matches(goals_scored)
        goalsConcededAvg = self.get_goals_conceded_avg_in_matches(goals_conceded)
        goalsScoredStddev = self.get_goals_scored_stddev_in_matches(goals_scored)
        goalsConcededStddev = self.get_goals_conceded_stddev_in_matches(goals_conceded)
        daysSinceLastMatch = float(int((date - current_form['last_date']).days))
        return [winPct, drawPct, goalsScoredAvg, goalsConcededAvg, goalsScoredStddev, goalsConcededStddev, daysSinceLastMatch], True

    def get_mean_last_two_seasons(self, filtered_history, match_attr: str):
        filtered_attr = filtered_history[match_attr]
        return filtered_attr.mean()

    def get_std_last_two_seasons(self, filtered_history, match_attr: str):
        filtered_attr = filtered_history[match_attr]
        std_sample = filtered_attr.std()
        return 0 if np.isnan(std_sample) else std_sample

    def init_long_term_dict(s, scored_goals, conceded_goals, win, draw, where):
        return {'matches': 1,
                'scored': scored_goals,
                'conceded': conceded_goals,
                'wins': win,
                'draws': draw,
                'scoredSq': scored_goals*scored_goals,
                'concededSq': conceded_goals*conceded_goals}

    def update_team_long_term_form(s, season, team_id, lid, scored, conceded, win, draw, where):  # todo remove this comment todo remove this comment todo remove this comment
        lt_tag = get_lt_tag(lid, team_id)
        long_term_tag = 'long-term'
        if lt_tag not in s.team_features:
            s.team_features[lt_tag] = {long_term_tag: {(season, where): s.init_long_term_dict(scored, conceded, win,
                                                                                              draw, where)}}
        elif long_term_tag not in s.team_features[lt_tag]:
            s.team_features[lt_tag][long_term_tag] = {(season, where): s.init_long_term_dict(scored, conceded, win,
                                                                                             draw, where)}
        elif (season, where) not in s.team_features[lt_tag][long_term_tag]:
            s.team_features[lt_tag][long_term_tag][season, where] = s.init_long_term_dict(scored, conceded, win, draw,
                                                                                          where)
        else:
            s.team_features[lt_tag][long_term_tag][season, where]['matches'] += 1
            s.team_features[lt_tag][long_term_tag][season, where]['scored'] += scored
            s.team_features[lt_tag][long_term_tag][season, where]['scoredSq'] += scored*scored
            s.team_features[lt_tag][long_term_tag][season, where]['conceded'] += conceded
            s.team_features[lt_tag][long_term_tag][season, where]['concededSq'] += conceded*conceded
            s.team_features[lt_tag][long_term_tag][season, where]['wins'] += win
            s.team_features[lt_tag][long_term_tag][season, where]['draws'] += draw

        long_term = s.team_features[lt_tag][long_term_tag]
        n_matches = 0
        n_wins, n_draws, n_scored, n_conceded, n_scoredSq, n_concededSq = (0 for _ in range(6))
        seasons = [long_term[sea, where] for sea in range(season - 1, season + 1) if (sea, where) in long_term]
        for sea in seasons:
            n_matches += sea['matches']
            n_wins += sea['wins']
            n_draws += sea['draws']
            n_scored += sea['scored']
            n_scoredSq += sea['scoredSq']
            n_conceded += sea['conceded']
            n_concededSq += sea['concededSq']
        if n_matches >= PARAMS['num_team_matches_to_ignore']:
            winPct = n_wins / n_matches
            drawPct = n_draws / n_matches
            scored_avg = n_scored / n_matches
            scored_std = np.sqrt(naive_variance(sum_sq=n_scoredSq, sum=n_scored, n=n_matches))
            conceded_avg = n_conceded / n_matches
            conceded_std = np.sqrt(naive_variance(sum_sq=n_concededSq, sum=n_conceded, n=n_matches))
            if 'long-term-features' not in s.team_features[lt_tag]:
                s.team_features[lt_tag]['long-term-features'] = dict()
            s.team_features[lt_tag]['long-term-features'][lt_tag, where, season] = (winPct, drawPct, scored_avg, conceded_avg, scored_std, conceded_std)



    def update_long_term_form(self, season, HID, AID, LID, HSC, ASC, H, D, A):
        self.update_team_long_term_form(season, HID, LID, HSC, ASC, H, D, 'H')
        self.update_team_long_term_form(season, AID, LID, ASC, HSC, A, D, 'A')

    def get_last_two_seasons_features(self, teamID, leagueID, season, isHomeTeam):
        dummy_v = [0 for _ in range(6)]
        lt_tag = get_lt_tag(leagueID, teamID)
        where = "H" if isHomeTeam else "A"
        if 'long-term-features' not in self.team_features[lt_tag] or \
                (lt_tag, where, season) not in self.team_features[lt_tag]['long-term-features']:
            return dummy_v, False
        winPct, drawPct, gsAvg, gcAvg, gsStd, gcStd = self.team_features[lt_tag]['long-term-features'][lt_tag, where, season]
        # return [winPct, drawPct, gsAvg, gcAvg], True
        return [winPct, drawPct, gsAvg, gcAvg, gsStd, gcStd], True

    def get_pi_features(self, home_teamID, away_teamID, leagueID):
        bet = True
        X = 0.2
        home_lt_tag = get_lt_tag(leagueID, home_teamID)
        away_lt_tag = get_lt_tag(leagueID, away_teamID)
        if home_lt_tag not in self.team_features or 'PI_RATINGS' not in self.team_features[home_lt_tag]:
            bet = False
            self.team_features[home_lt_tag]['PI_RATINGS'] = (X, -X)

        if away_lt_tag not in self.team_features or 'PI_RATINGS' not in self.team_features[away_lt_tag]:
            bet = False
            self.team_features[away_lt_tag]['PI_RATINGS'] = (X, -X)

        home_pi_H, home_pi_A = self.team_features[home_lt_tag]['PI_RATINGS']
        away_pi_H, away_pi_A = self.team_features[away_lt_tag]['PI_RATINGS']
        return [home_pi_H, away_pi_A, self.pi.get_expected_goal_diff(home_pi_H, away_pi_A)], bet

    def update_pi_ratings(self, home_teamID, away_teamID, leagueID, goal_diff):
        home_lt_tag = get_lt_tag(leagueID, home_teamID)
        away_lt_tag = get_lt_tag(leagueID, away_teamID)
        # home_pi_H, home_pi_A = self.team_features[home_lt_tag]['PI_RATINGS']
        # away_pi_H, away_pi_A = self.team_features[away_lt_tag]['PI_RATINGS']
        hph, hpa, aph, apa = self.pi.get_updated_pi_ratings(*self.team_features[home_lt_tag]['PI_RATINGS'],
                                                            *self.team_features[away_lt_tag]['PI_RATINGS'],
                                                            goal_diff)
        self.team_features[home_lt_tag]['PI_RATINGS'] = (hph, hpa)
        self.team_features[away_lt_tag]['PI_RATINGS'] = (aph, apa)

    def should_train_now(self, last_inc_date):
        return (self.last_trained_date is None) or (last_inc_date - self.last_trained_date).days > self.train_every_n_days

    def calc_team_feature(self, teamID, leagueID, date, season, isHomeTeam):
        # based on H_HISTORY, A_HISTORY
        isValidFV = True
        v = []
        history_features_h, history_features_h_valid = self.get_last_two_seasons_features(teamID, leagueID, season,
                                                                                          isHomeTeam)
        history_features_a, history_features_a_valid = self.get_last_two_seasons_features(teamID, leagueID, season,
                                                                                          not isHomeTeam)
        isValidFV &= history_features_h_valid
        isValidFV &= history_features_a_valid
        current_form_features, form_features_valid = self.get_current_form(teamID, leagueID, date)
        isValidFV &= form_features_valid
        v += history_features_h
        v += history_features_a
        v += current_form_features
        return v, isValidFV

    def update_league(s, cur_season, lid, hsc, asc, h, d, hid, aid):
        # todo missing team_count
        gd = np.abs(hsc - asc)
        if lid not in s.league_features or cur_season not in s.league_features[lid]:
            s.league_features[lid][cur_season] = {'matches': 1,
                                                  'hsc': hsc,
                                                  'asc': asc,
                                                  'h': h,
                                                  'd': d,
                                                  'hscSq': hsc*hsc,
                                                  'ascSq': asc*asc,
                                                  'gd': gd,
                                                  'gdSq': gd*gd,
                                                  'teams': {hid, aid}}
        else:
            s.league_features[lid][cur_season]['matches'] += 1
            s.league_features[lid][cur_season]['hsc'] += hsc
            s.league_features[lid][cur_season]['asc'] += asc
            s.league_features[lid][cur_season]['gd'] += gd
            s.league_features[lid][cur_season]['hscSq'] += hsc*hsc
            s.league_features[lid][cur_season]['ascSq'] += asc*asc
            s.league_features[lid][cur_season]['gdSq'] += gd*gd
            s.league_features[lid][cur_season]['h'] += h
            s.league_features[lid][cur_season]['d'] += d
            s.league_features[lid][cur_season]['teams'].update([hid, aid])

        league = s.league_features[lid]
        n_matches = 0
        n_hsc, n_asc, n_gd, n_hscSq, n_ascSq, n_gdSq, n_h, n_d = (0 for _ in range(8))
        seasons = [league[season] for season in range(cur_season - 2, cur_season) if season in league]
        for season in seasons:
            n_matches += season['matches']
            n_hsc += season['hsc']
            n_asc += season['asc']
            n_gd += season['gd']
            n_hscSq += season['hscSq']
            n_ascSq += season['ascSq']
            n_gdSq += season['gdSq']
            n_h += season['h']
            n_d += season['d']
        if n_matches >= PARAMS['num_league_matches_to_ignore'] and (cur_season-1) in league:
            hscm = n_hsc / n_matches
            ascm = n_asc / n_matches
            hscDev = np.sqrt(naive_variance(sum_sq=n_hscSq, sum=n_hsc, n=n_matches))
            ascDev = np.sqrt(naive_variance(sum_sq=n_ascSq, sum=n_asc, n=n_matches))
            gdDev = np.sqrt(naive_variance(sum_sq=n_gdSq, sum=n_gd, n=n_matches))
            hm = n_h / n_matches
            dm = n_d / n_matches
            teams_last_season = len(league[cur_season-1]['teams'])
            s.league_features[lid][cur_season, 'stats'] = (hscm, ascm, hscDev, ascDev, hm, dm, teams_last_season, gdDev)
            # s.long_term_last_match_processed[lt_tag, where, cur_season] = match_ID

    def calc_league_feature_slow(self, leagueID, date, season):
        isValidFV = True
        dummy_v = [0, 0, 0, 0, 0, 0, 0, 0]

        if 'L_HISTORY' not in self.league_features[leagueID]:
            return dummy_v, False

        l_hist = self.league_features[leagueID]['L_HISTORY']
        then_seas = int(season) - 2
        l_hist_l2 = l_hist.query(f'Sea >= {then_seas} & Sea < {season}')

        if l_hist_l2.empty:
            return dummy_v, False

        # if (season, 'stats') not in self.league_features[leagueID]:
        #     return dummy_v, False
        # hscm, ascm, hm, dm = self.league_features[leagueID][season, 'stats']

        h_gs_avg = self.get_mean_last_two_seasons(filtered_history=l_hist_l2, match_attr='HSC')
        a_gs_avg = self.get_mean_last_two_seasons(filtered_history=l_hist_l2, match_attr='ASC')
        h_gs_std = self.get_std_last_two_seasons(filtered_history=l_hist_l2, match_attr='HSC')
        a_gs_std = self.get_std_last_two_seasons(filtered_history=l_hist_l2, match_attr='ASC')
        h_win_pct = self.get_mean_last_two_seasons(filtered_history=l_hist_l2, match_attr='H')
        draw_pct = self.get_mean_last_two_seasons(filtered_history=l_hist_l2, match_attr='D')
        team_cnt = l_hist_l2['HID'].append(l_hist_l2['AID']).unique().size
        gd_std = np.abs(l_hist_l2['HSC'] - l_hist_l2['ASC']).std()

        # print(f'hscm {h_gs_avg:.4f} {hscm:.4f} ascm {a_gs_avg:.4f} {ascm:.4f} hwin {h_win_pct:.4f} {hm:.4f} draw {draw_pct:.4f} {dm:.4f}')

        if np.isnan(gd_std):
            # todo make feature vector invalid?
            gd_std = 0  # TODO: replacing NaN by 0?

        features = [h_gs_avg, a_gs_avg, h_gs_std, a_gs_std, h_win_pct, draw_pct, team_cnt, gd_std]
        # start_date = pd.Timestamp('2005-08-01')
        # if date > start_date:
        #     print(features)

        return features, isValidFV


    def calc_league_feature_fast(self, leagueID, date, season):
        # isValidFV = True
        dummy_v = [0 for _ in range(8)]

        if (season, 'stats') not in self.league_features[leagueID]:
            return dummy_v, False
        features = list(self.league_features[leagueID][season, 'stats'])
        # start_date = pd.Timestamp('2005-08-01')
        # if date > start_date:
        #     print(features)

        return features, True

    def update_league_table(self, HID, AID, LID, season, H, D, A):
        if 'League_Tables' not in self.league_features[LID]:
            self.league_features[LID]['League_Tables'] = dict()
        league_tables = self.league_features[LID]['League_Tables']
        if season not in league_tables:
            league_tables[season] = dict()
        if (season, 'matchCount') not in league_tables:
            league_tables[(season, 'matchCount')] = dict()
        cur_league_matches_played = league_tables[(season, 'matchCount')]
        cur_league_table = league_tables[season]
        h_score = PARAMS['points_for_win']*int(H) + PARAMS['points_for_loss']*int(D)
        a_score = PARAMS['points_for_win']*int(A) + PARAMS['points_for_loss']*int(D)
        cur_league_table[HID] = cur_league_table.get(HID, 0) + h_score
        cur_league_table[AID] = cur_league_table.get(AID, 0) + a_score
        cur_league_matches_played[HID] = cur_league_matches_played.get(HID, 0) + 1
        cur_league_matches_played[AID] = cur_league_matches_played.get(AID, 0) + 1

    def get_match_importance(self, teamA, teamB, leagueID, season):
        r_size = PARAMS['num_top_and_bottom_league_table_teams_for_stats']
        dummy_vec = [0 for _ in range((4 * r_size) + 1)]
        if 'League_Tables' not in self.league_features[leagueID]:
            return dummy_vec, False
        league_tables = self.league_features[leagueID]['League_Tables']
        if season not in league_tables:
            return dummy_vec, False
        if (season, 'matchCount') not in league_tables:
            return dummy_vec, False
        league_matches_played = league_tables[(season, 'matchCount')]
        league_table = league_tables[season]
        league_points = np.array(sorted(league_table.values(), reverse=True))
        if league_points.shape[0] < PARAMS['minimal_league_table_size']:
            return dummy_vec, False
        teamA_n_matches = league_matches_played.get(teamA, 1)
        teamB_n_matches = league_matches_played.get(teamB, 1)
        teamA_top_diffs = ((league_table.get(teamA, 0) - league_points[:r_size])/teamA_n_matches).tolist()
        teamA_bot_diffs = ((league_table.get(teamA, 0) - league_points[-r_size:])/teamA_n_matches).tolist()
        teamB_top_diffs = ((league_table.get(teamB, 0) - league_points[:r_size])/teamB_n_matches).tolist()
        teamB_bot_diffs = ((league_table.get(teamB, 0) - league_points[-r_size:])/teamB_n_matches).tolist()
        return teamA_top_diffs + teamA_bot_diffs + teamB_top_diffs + teamB_bot_diffs + [max(teamA_n_matches, teamB_n_matches)], True

    def calc_match_importance_features(self,  teamA, teamB, leagueID, date, season) -> Tuple[list, bool]:
        return self.get_match_importance(teamA, teamB, leagueID, season)

    def get_sample_weight(self, strategy, num_samples):
        if strategy == SampleWeightStrategy.NONE:
            return None
        if strategy == SampleWeightStrategy.LINEAR:
            weights = np.arange(num_samples)
            weights = weights / np.linalg.norm(weights)
            return weights
        if strategy == SampleWeightStrategy.BETA:
            h, e = np.histogram(np.random.beta(a=PARAMS['GBC_beta_distribution_params']['alpha'],
                                               b=PARAMS['GBC_beta_distribution_params']['beta'],
                                               size=num_samples), bins=num_samples)
            return h / np.linalg.norm(h)
        return None

    def place_bets(self, opps: pd.DataFrame, summary, inc):
        # print(opps)
        self.update(inc)
        not_enough_data_for_feature = 0
        for index, match in inc.iterrows():
            Sea, Date, LID, HID, AID, HSC, ASC, H, D, A = match[['Sea', 'Date', 'LID', 'HID', 'AID', 'HSC', 'ASC', 'H', 'D', 'A']]
            lf, lfVal = self.calc_league_feature(LID, Date, Sea)
            hf, hfVal = self.calc_team_feature(HID, LID, Date, Sea, isHomeTeam=True)
            af, afVal = self.calc_team_feature(AID, LID, Date, Sea, isHomeTeam=False)
            pif, pifVal = self.get_pi_features(HID, AID, LID)
            self.update_pi_ratings(HID, AID, LID, HSC - ASC)
            mf, mfVal = self.calc_match_importance_features(teamA=HID, teamB=AID, leagueID=LID, date=Date, season=Sea)
            self.update_league_table(HID, AID, LID, Sea, H, D, A)
            self.update_current_form(Date, HID, AID, LID, HSC, ASC, H, D, A)
            self.update_long_term_form(Sea, HID, AID, LID, HSC, ASC, H, D, A)
            self.update_league(Sea, LID, HSC, ASC, H, D, HID, AID)
            if not (lfVal and hfVal and afVal and pifVal and mfVal):
            # if not (lfVal and hfVal and afVal and pifVal):
                not_enough_data_for_feature += 1
            else:
                feature_vector = list(hf) + list(af) + list(lf) + list(pif) + list(mf)
                self.history_in_features += [feature_vector]
                self.historical_results += [int(self.get_result_classes_from_onehot(match))]
        assert len(self.historical_results) == len(self.history_in_features),\
            "Historical results (y) and historical features (X) must have same length!"

        if VERBOSE:
            print(f'Not enough data for creating feature vector for {not_enough_data_for_feature} matches out of '
                  f'{inc.shape[0]}')

        last_added_date = inc['Date'].iat[-1] if not inc.empty else self.last_trained_date

        if not inc.empty and self.should_train_now(last_inc_date=last_added_date):
            if VERBOSE:
                print(f"({time() - self.start_time:7.2f} s) | Training on {last_added_date}")
            self.model.fit(np.array(self.history_in_features), np.array(self.historical_results),
                           sample_weight=self.get_sample_weight(PARAMS['GBC_sample_weight'], len(self.history_in_features)))
            self.last_trained_date = last_added_date
            # self.stats.print()

        # probabilities = gbt.predict(opps -> feature_vector)
        matches_to_predict, validity_of_fv = self.get_feature_vectors(opps[['Sea', 'Date', 'LID', 'HID', 'AID']])
        prediction_probs = self.model.predict_proba(matches_to_predict)
        # print(f'prediction probs {prediction_probs}')
        bm_probs = 1 / np.array(opps[['OddsH', 'OddsD', 'OddsA']])
        bets = self.bet(prediction_probs, bm_probs, resources=summary['Bankroll'].item(),
                        min_bet=summary.iloc[0].to_dict()['Min_bet'],
                        max_bet=summary.iloc[0].to_dict()['Max_bet'],
                        strategy=PARAMS['bet_strategy_type'])
        # self.stats.update(opps, prediction_probs, bm_probs, bets, inc, np.array(opps[['OddsH', 'OddsD', 'OddsA']]))

        return pd.DataFrame(data=bets, columns=['BetH', 'BetD', 'BetA'], index=opps.index)

    def update_team_history(self, teamID, leagueID, matches, where):
        history_label = f'{where}_HISTORY'

        lt_tag = get_lt_tag(leagueID, teamID)
        if history_label not in self.team_features[lt_tag].keys():
            self.team_features[lt_tag][history_label] = matches
        else:
            self.team_features[lt_tag][history_label] = self.team_features[lt_tag][history_label].append(matches)

    def get_result_classes_from_onehot(self, onehot_df):
        """
        Takes a dataframe with [H, D, A] columns, (which are onehot encoding of results) and returns them as classes,
        with H=0, D=1, A=2
        :param onehot_df:
        :return:
        """
        return (onehot_df[['H', 'D', 'A']].to_numpy().reshape(-1, 3) * np.array([0, 1, 2])).sum(axis=1)

    def get_feature_vectors(self, IDs_df: pd.DataFrame):
        """
        Dataframe with columns [Date, LID, HID, AID]
        :param IDs_df:
        :return:
        """
        #TODO: Do we need Date column? We don't know!
        n_matches = len(IDs_df.index)
        n_features = 2*self.n_team_features + self.n_league_features + self.n_pi_features + self.n_match_importance_features

        features = np.empty((n_matches, n_features), dtype=float)
        validity = np.empty((n_matches), dtype=bool)
        for i, (index, row) in enumerate(IDs_df.iterrows()):
            if index not in self.match2feature_vector:
                date = row['Date']
                season = row['Sea']
                hid = row['HID']
                aid = row['AID']
                lid = row['LID']
                features[i, :self.n_team_features], hfVal = self.calc_team_feature(hid, lid, date, season, True)
                features[i, self.n_team_features:2 * self.n_team_features], afVal = self.calc_team_feature(aid, lid, date,
                                                                                                           season,
                                                                                                           False)
                features[i, 2 * self.n_team_features:2 * self.n_team_features + self.n_league_features], lfVal = \
                    self.calc_league_feature(lid, date, season)
                features[i,
                2 * self.n_team_features + self.n_league_features:2 * self.n_team_features + self.n_league_features + self.n_pi_features], pifVal = \
                    self.get_pi_features(hid, aid, lid)
                features[i, 2 * self.n_team_features + self.n_league_features + self.n_pi_features:], miVal = \
                    self.calc_match_importance_features(teamA=hid,
                                                        teamB=aid,
                                                        leagueID=lid,
                                                        date=date,
                                                        season=season)

                validity[i] = hfVal and afVal and lfVal and pifVal and miVal

                self.match2feature_vector[index] = features[i, :], validity[i]
            else:
                features[i, :], validity[i] = self.match2feature_vector[index]

        return features, validity

    def bet(self, prediction_probs, bm_probs, resources, min_bet, max_bet, strategy):
        diff = prediction_probs - bm_probs
        bets = np.zeros(shape=prediction_probs.shape)

        if strategy == BettingStrategy.AGGRESSIVE:  # aggressive
            THRESHOLD = PARAMS['bet_aggressive_probability_bound']
            diff[diff <= THRESHOLD] = 0
            if np.count_nonzero(diff[diff > THRESHOLD]) == 0:
                diff[:, :] = 0
                return diff
            bet = min(max_bet, math.floor(resources / np.count_nonzero(diff[diff > THRESHOLD])))
            diff[diff > THRESHOLD] = bet
            bets = diff
        elif strategy == BettingStrategy.LESS_AGGRESSIVE:  # less aggressive
            THRESHOLD = PARAMS['bet_less_aggressive_probability_bounds'][0 if resources < PARAMS['bet_ratio_resources_bound'] else 1]
            diff[diff <= THRESHOLD] = 0
            if np.count_nonzero(diff[diff > THRESHOLD]) == 0:
                diff[:, :] = 0
                bets = diff
            else:
                max_bet_sum = resources // (PARAMS['bet_ratio_small'] if resources < PARAMS['bet_ratio_resources_bound'] else PARAMS['bet_ratio_large'])

                bet = min(max_bet, math.floor(max_bet_sum / np.count_nonzero(diff[diff > THRESHOLD])))
                if bet < min_bet:
                    bet = min_bet
                    available = max_bet_sum
                    for i in range(len(prediction_probs)):
                        for j in range(3):
                            if diff[i, j] > 0:
                                diff[i, j] = bet
                                available -= bet
                            if available < bet:
                                bet = 0
                    bets = diff
                else:
                    diff[diff > THRESHOLD] = bet
                    bets = diff
        elif strategy == BettingStrategy.COMPLEX:
            LB, UB = 1.1, 2
            odds = 1/bm_probs
            ratios = prediction_probs / bm_probs
            ratios[ratios < LB] = 0
            ratios[ratios > UB] = UB
            ratios[odds >= 10.0] = 0
            max_bet_sum = resources // (PARAMS['bet_ratio_small'] if resources < PARAMS['bet_ratio_resources_bound']
                                        else PARAMS['bet_ratio_large'])
            matches_to_bet_on = (ratios.sum(1) > 0).nonzero()[0]
            n_matches_to_bet_on = matches_to_bet_on.shape[0]
            opps_to_bet_on = (ratios > 0).nonzero()
            # n_opps_to_bet_on = opps_to_bet_on[0].shape[0]
            # todo if H and A -> 0
            avg_ratios = [ratios[i][ratios[i] > 0].mean() for i in matches_to_bet_on]
            if max_bet_sum < min_bet * n_matches_to_bet_on:
                matches_to_bet_on = np.argsort(ratios.max(1))[:int(max_bet_sum // (min_bet*2) + 1)]
                # n_matches_to_bet_on = len(matches_to_bet_on)

            unit = (max_bet_sum - min_bet) / sum(avg_ratios)

            for i in matches_to_bet_on:
                if np.count_nonzero(ratios[i]) > 1:
                    if ratios[i, 1] == 0:
                        continue
                    odds = (1 / bm_probs)
                    odds2 = odds[i][ratios[i].nonzero()[0]]
                    # print(odds2)
                    bet = min_bet + sum(unit * ratios[i]) / 2
                    x = (bet * odds2[1]) / sum(odds2)
                    # print(bet, x, ratios[i])
                    bets[i][ratios[i] > 0] = np.array(list(map(lambda q: max(min_bet, int(q)), [x, bet - x])))
                else:
                    bets[i][ratios[i] > 0] = (min_bet + unit * ratios[i][ratios[i] > 0]).astype(int)


        else:
            print('INVALID BETTING STRATEGY', file=sys.stderr)
            exit(1)

        # # print(f'bets:\n{bets}')
        # print('pred/bm/bet\n-----------------')
        # for i in range(len(prediction_probs)):
        #     for l in (prediction_probs[i, :], bm_probs[i, :], bets[i, :]):
        #         for j in range(3):
        #             print(f'{l[j]:.3f} ', end='')
        #         print()
        #     print('-----------------')
        bets[bets > max_bet] = max_bet
        return bets
