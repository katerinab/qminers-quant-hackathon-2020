import math
from typing import Tuple

import numpy as np
import pandas as pd
from collections import defaultdict
from sklearn.ensemble import GradientBoostingClassifier
import sys
# import stats as st
from time import time


class PiRatingCalculator:
    def __init__(self, LEARNING_RATE_LAMBDA=0.06, LEARNING_RATE_GAMMA=0.5, LOG_BASE=10, C=3):
        self.LEARNING_RATE_LAMBDA = LEARNING_RATE_LAMBDA
        self.LEARNING_RATE_GAMMA = LEARNING_RATE_GAMMA
        self.LOG_BASE = LOG_BASE
        self.C = C

    def get_expected_goal_diff(self, home_pi_rating, away_pi_rating):
        # expected goal difference against average team
        a = np.array([home_pi_rating, away_pi_rating])
        gd = np.sign(a) * (np.power(self.LOG_BASE, np.abs(a) / self.C) - 1)
        return gd[0] - gd[1]

    def weighted_error(self, goal_diff, expected_goal_diff):
        # returns weighted home error, weighted away error
        error = np.abs(goal_diff - expected_goal_diff)
        # c*logB(1+e)
        we = (1 if expected_goal_diff < goal_diff else -1) * self.C * np.log(1 + error) / np.log(self.LOG_BASE)
        return we, -we

    def get_updated_pi_ratings(self, home_pi_H, home_pi_A, away_pi_H, away_pi_A, goal_diff):
        weh, wea = self.weighted_error(goal_diff,
                                       self.get_expected_goal_diff(home_pi_H, away_pi_A)
                                       # doesn't have to be computed again
                                       )
        new_home_pi_H = home_pi_H + weh * self.LEARNING_RATE_LAMBDA
        new_home_pi_A = home_pi_A + (new_home_pi_H - home_pi_H) * self.LEARNING_RATE_GAMMA
        new_away_pi_A = away_pi_A + wea * self.LEARNING_RATE_LAMBDA
        new_away_pi_H = away_pi_H + (new_away_pi_A - away_pi_A) * self.LEARNING_RATE_GAMMA
        return new_home_pi_H, new_home_pi_A, new_away_pi_H, new_away_pi_A


class Model:
    model: GradientBoostingClassifier

    def __init__(self, train_every_n_days=90):
        # feature dicts indexed by team id, league id
        self.team_features = defaultdict(dict)
        # {team1: {avgGoals: 0, maxGoals: 1}, team2: {avgGoals: 0, maxGoals: 1}}
        self.league_features = defaultdict(dict)
        self.model = GradientBoostingClassifier(max_depth=5, subsample=0.8, n_estimators=40)
        self.history_in_features = []
        self.historical_results = []
        self.n_team_features = 12 + 6 + 1
        self.n_league_features = 8
        self.n_pi_features = 3
        self.n_match_importance_features = 20
        self.last_trained_date = None
        self.train_every_n_days = train_every_n_days
        # self.stats = st.Stats()
        self.start_time = time()
        self.pi = PiRatingCalculator()

    def update(self, new_results: pd.DataFrame):
        # update team features
        for j in ['HID', 'AID']:
            for home_team_ID in new_results[j].unique():
                home_matches = new_results[new_results[j] == home_team_ID]
                self.update_team_history(home_team_ID, home_matches, j[0])

        for team_ID in new_results['HID'].append(new_results['AID']).unique():
            matches = new_results.loc[(new_results['HID'] == team_ID) | (new_results['AID'] == team_ID)]
            self.update_team_history(team_ID, matches, 'F')

        for league_ID in new_results['LID'].unique():
            matches = new_results.loc[(new_results['LID'] == league_ID)]
            history_label = f'L_HISTORY'
            if history_label not in self.league_features[league_ID].keys():
                self.league_features[league_ID][history_label] = matches
            else:
                self.league_features[league_ID][history_label] = self.league_features[league_ID][history_label].append(matches)

    def get_winPct_in_matches(self, teamID: int, matches: pd.DataFrame):
        wins_when_home = matches[matches['HID'] == teamID]['H'].sum()
        wins_when_away = matches[matches['AID'] == teamID]['A'].sum()

        return (wins_when_home + wins_when_away)/len(matches.index)

    def get_drawPct_in_matches(self, matches: pd.DataFrame):
        return matches['D'].mean()

    def get_goals_scored_avg_in_matches(self, goals_scored: np.ndarray):
       return goals_scored.mean()

    def get_goals_conceded_avg_in_matches(self, goals_conceded: np.ndarray):
        return goals_conceded.mean()

    def get_goals_scored_stddev_in_matches(self, goals_scored: np.ndarray):
        stddev = goals_scored.std()
        return 0.0 if np.isnan(stddev) else stddev

    def get_goals_conceded_stddev_in_matches(self, goals_conceded: np.ndarray):
        stddev = goals_conceded.std()
        return 0.0 if np.isnan(stddev) else stddev

    def get_scored_conceded_goals_in_matches(self, teamID, matches: pd.DataFrame) -> Tuple[np.ndarray, np.ndarray]:
        matches_where_home = matches[matches['HID'] == teamID]
        matches_where_away = matches[matches['AID'] == teamID]
        goals_scored = np.concatenate((matches_where_home['HSC'].to_numpy(), matches_where_away['ASC'].to_numpy()))
        goals_conceded = np.concatenate((matches_where_home['ASC'].to_numpy(), matches_where_away['HSC'].to_numpy()))
        return goals_scored, goals_conceded

    def get_scored_goals_in_matches(self, teamID: int, matches: pd.DataFrame) -> np.ndarray:
        # TODO: remove?
        goals_when_home = matches[matches['HID'] == teamID]['HSC'].to_numpy()
        goals_when_away = matches[matches['AID'] == teamID]['ASC'].to_numpy()
        return np.concatenate((goals_when_home, goals_when_away))

    def get_conceded_goals_in_matches(self, teamID: int, matches: pd.DataFrame) -> np.ndarray:
        # TODO: remove?
        goals_when_home = matches[matches['HID'] == teamID]['ASC'].to_numpy()
        goals_when_away = matches[matches['AID'] == teamID]['HSC'].to_numpy()
        return np.concatenate((goals_when_home, goals_when_away))

    def get_days_since_last_match_from_matches(self, date, matches):
        return float(int((date - matches.tail(n=1)['Date']).dt.days))  # maybe float straight away? Not sure rn

    def get_current_form(self, teamID, date):
        dummy_vec = [0.0 for _ in range(7)]
        if 'F_HISTORY' in self.team_features[teamID]:
            full_team_hist = self.team_features[teamID]['F_HISTORY']
            last_five = full_team_hist[full_team_hist['Date'] < date].tail(n=5)
            if last_five.empty:
                return dummy_vec, False

            goals_scored, goals_conceded = self.get_scored_conceded_goals_in_matches(teamID, last_five)

            winPct = self.get_winPct_in_matches(teamID, last_five)
            drawPct = self.get_drawPct_in_matches(last_five)
            goalsScoredAvg = self.get_goals_scored_avg_in_matches(goals_scored)
            goalsConcededAvg = self.get_goals_conceded_avg_in_matches(goals_conceded)
            goalsScoredStddev = self.get_goals_scored_stddev_in_matches(goals_scored)
            goalsConcededStddev = self.get_goals_conceded_stddev_in_matches(goals_conceded)
            daysSinceLastMatch = self.get_days_since_last_match_from_matches(date, last_five)
            return [winPct, drawPct, goalsScoredAvg, goalsConcededAvg, goalsScoredStddev, goalsConcededStddev, daysSinceLastMatch], True
        else:
            return dummy_vec, False

    def get_mean_last_two_seasons(self, filtered_history, match_attr: str):
        filtered_attr = filtered_history[match_attr]
        return filtered_attr.mean()

    def get_std_last_two_seasons(self, filtered_history, match_attr: str):
        filtered_attr = filtered_history[match_attr]
        std_sample = filtered_attr.std()
        return 0 if np.isnan(std_sample) else std_sample

    def get_last_two_seasons_features(self, teamID, season, isHomeTeam, date):
        dummy_v = [0, 0, 0, 0, 0, 0]
        if isHomeTeam:
            if teamID not in self.team_features or 'H_HISTORY' not in self.team_features[teamID]:
                return dummy_v, False
        elif teamID not in self.team_features or 'A_HISTORY' not in self.team_features[teamID]:
            return dummy_v, False

        filtered_history = self.team_features[teamID]['H_HISTORY' if isHomeTeam else 'A_HISTORY']
        then_seas = int(season) - 2
        filtered_time = filtered_history.query(f'Sea > {then_seas} & Sea <= {season}')
        filtered_time = filtered_time.loc[filtered_time['Date'] < date]

        if filtered_time.empty:
            return dummy_v, False

        winPct = self.get_mean_last_two_seasons(filtered_history=filtered_time, match_attr='H' if isHomeTeam else 'A')
        drawPct = self.get_mean_last_two_seasons(filtered_history=filtered_time, match_attr='D')
        gsAvg = self.get_mean_last_two_seasons(filtered_history=filtered_time, match_attr='HSC' if isHomeTeam else 'ASC')
        gcAvg = self.get_mean_last_two_seasons(filtered_history=filtered_time, match_attr='ASC' if isHomeTeam else 'HSC')
        gsStd = self.get_std_last_two_seasons(filtered_history=filtered_time, match_attr='HSC' if isHomeTeam else 'ASC')
        gcStd = self.get_std_last_two_seasons(filtered_history=filtered_time, match_attr='ASC' if isHomeTeam else 'HSC')

        return [winPct, drawPct, gsAvg, gcAvg, gsStd, gcStd], True

    def get_pi_features(self, home_teamID, away_teamID):
        bet = True
        X = 0.2
        if home_teamID not in self.team_features or 'PI_RATINGS' not in self.team_features[home_teamID]:
            bet = False
            self.team_features[home_teamID]['PI_RATINGS'] = (X, -X)

        if away_teamID not in self.team_features or 'PI_RATINGS' not in self.team_features[away_teamID]:
            bet = False
            self.team_features[away_teamID]['PI_RATINGS'] = (X, -X)

        home_pi_H, home_pi_A = self.team_features[home_teamID]['PI_RATINGS']
        away_pi_H, away_pi_A = self.team_features[away_teamID]['PI_RATINGS']
        return [home_pi_H, away_pi_A, self.pi.get_expected_goal_diff(home_pi_H, away_pi_A)], bet

    def update_pi_ratings(self, home_teamID, away_teamID, goal_diff):
        # home_pi_H, home_pi_A = self.team_features[home_teamID]['PI_RATINGS']
        # away_pi_H, away_pi_A = self.team_features[away_teamID]['PI_RATINGS']
        hph, hpa, aph, apa = self.pi.get_updated_pi_ratings(*self.team_features[home_teamID]['PI_RATINGS'],
                                                            *self.team_features[away_teamID]['PI_RATINGS'],
                                                            goal_diff)
        self.team_features[home_teamID]['PI_RATINGS'] = (hph, hpa)
        self.team_features[away_teamID]['PI_RATINGS'] = (aph, apa)

    def should_train_now(self, last_inc_date):
        return (self.last_trained_date is None) or (last_inc_date - self.last_trained_date).days > self.train_every_n_days


    def calc_team_feature(self, teamID, date, season, isHomeTeam):
        # based on H_HISTORY, A_HISTORY
        isValidFV = True
        v = []
        history_features_h, history_features_h_valid = self.get_last_two_seasons_features(teamID, season, isHomeTeam, date)
        history_features_a, history_features_a_valid = self.get_last_two_seasons_features(teamID, season, not isHomeTeam, date)
        isValidFV &= history_features_h_valid
        isValidFV &= history_features_a_valid
        current_form_features, form_features_valid = self.get_current_form(teamID, date)
        isValidFV &= form_features_valid
        v += history_features_h
        v += history_features_a
        v += current_form_features
        return v, isValidFV

    def calc_league_feature(self, leagueID, date, season):
        isValidFV = True
        dummy_v = [0, 0, 0, 0, 0, 0, 0, 0]

        if 'L_HISTORY' not in self.league_features[leagueID]:
            return dummy_v, False

        l_hist = self.league_features[leagueID]['L_HISTORY']
        then_seas = int(season) - 2
        l_hist_l2 = l_hist.query(f'Sea > {then_seas} & Sea <= {season}')

        if l_hist_l2.empty:
            return dummy_v, False

        h_gs_avg = self.get_mean_last_two_seasons(filtered_history=l_hist_l2, match_attr='HSC')
        a_gs_avg = self.get_mean_last_two_seasons(filtered_history=l_hist_l2, match_attr='ASC')
        h_gs_std = self.get_std_last_two_seasons(filtered_history=l_hist_l2, match_attr='HSC')
        a_gs_std = self.get_std_last_two_seasons(filtered_history=l_hist_l2, match_attr='ASC')
        h_win_pct = self.get_mean_last_two_seasons(filtered_history=l_hist_l2, match_attr='H')
        draw_pct = self.get_mean_last_two_seasons(filtered_history=l_hist_l2, match_attr='D')
        team_cnt = l_hist_l2['HID'].append(l_hist_l2['AID']).unique().size
        gd_std = np.abs(l_hist_l2['HSC'] - l_hist_l2['ASC']).std()
        if np.isnan(gd_std):
            gd_std = 0  # TODO: replacing NaN by 0?

        return [h_gs_avg, a_gs_avg, h_gs_std, a_gs_std, h_win_pct, draw_pct, team_cnt, gd_std], isValidFV

    def update_league_table(self, HID, AID, LID, season, H, D, A):
        if not 'League_Tables' in self.league_features[LID]:
            self.league_features[LID]['League_Tables'] = dict()
        league_tables = self.league_features[LID]['League_Tables']
        if not season in league_tables:
            league_tables[season] = dict()
        cur_league_table = league_tables[season]
        h_score = 3*int(H) + 1*int(D)
        a_score = 3*int(A) + 1*int(D)
        cur_league_table[HID] = cur_league_table.get(HID, 0) + h_score
        cur_league_table[AID] = cur_league_table.get(AID, 0) + a_score

    def get_match_importance(self, teamA, teamB, leagueID, season):
        dummy_vec = [0 for _ in range(20)]
        if not 'League_Tables' in self.league_features[leagueID]:
            return dummy_vec, False
        league_tables = self.league_features[leagueID]['League_Tables']
        if not season in league_tables:
            return dummy_vec, False
        league_table = league_tables[season]
        league_points = np.array(sorted(league_table.values(), reverse=True))
        if league_points.shape[0] < 10:
            return dummy_vec, False
        teamA_top_diffs = (league_table.get(teamA, 0) - league_points[:5]).tolist()
        teamA_bot_diffs = (league_table.get(teamA, 0) - league_points[-5:]).tolist()
        teamB_top_diffs = (league_table.get(teamB, 0) - league_points[:5]).tolist()
        teamB_bot_diffs = (league_table.get(teamB, 0) - league_points[-5:]).tolist()
        return teamA_top_diffs + teamA_bot_diffs + teamB_top_diffs + teamB_bot_diffs, True

    def calc_match_importance_features(self,  teamA, teamB, leagueID, date, season) -> Tuple[list, bool]:
        return self.get_match_importance(teamA, teamB, leagueID, season)

    def calc_match_importance_features_old(self, teamA, teamB, leagueID, date, season) -> Tuple[list, bool]:
        dummy_vec = [0 for _ in range(20)]
        if 'L_HISTORY' in self.league_features[leagueID]:
            full_league_history = self.league_features[leagueID]['L_HISTORY']
            cur_league_season = full_league_history.loc[(full_league_history['Sea'] == season)]
            if cur_league_season.empty:
                return dummy_vec, False

            all_teams = cur_league_season['HID'].append(cur_league_season['AID']).unique()
            if len(all_teams) < 10:
                return dummy_vec, False

            cur_league_season_so_far = cur_league_season.loc[cur_league_season['Date'] < date]
            if len(cur_league_season_so_far.index) < 6:
                return dummy_vec, False

            so_far_teams = cur_league_season_so_far['HID'].append(cur_league_season_so_far['AID']).unique()
            league_table = {teamID: 0 for teamID in all_teams}

            for teamID in so_far_teams:
                home_matches = cur_league_season_so_far[cur_league_season_so_far['HID'] == teamID]
                home_points = 0 if home_matches.empty else (home_matches[['H', 'D']].to_numpy().reshape(-1, 2) * np.array([3, 1])).sum()
                away_matches = cur_league_season_so_far[cur_league_season_so_far['AID'] == teamID]
                away_points = 0 if away_matches.empty else (away_matches[['A', 'D']].to_numpy().reshape(-1, 2) * np.array([3, 1])).sum()
                league_table[teamID] = home_points + away_points

            league_points = np.array(sorted(league_table.values(), reverse=True))
            teamA_top_diffs = (league_table[teamA] - league_points[:5]).tolist()
            teamA_bot_diffs = (league_table[teamA] - league_points[-5:]).tolist()
            teamB_top_diffs = (league_table[teamB] - league_points[:5]).tolist()
            teamB_bot_diffs = (league_table[teamB] - league_points[-5:]).tolist()
            return teamA_top_diffs + teamA_bot_diffs + teamB_top_diffs + teamB_bot_diffs, True
        else:
            return dummy_vec, False  # TODO: maybe return true?


    def place_bets(self, opps, summary, inc):
        self.update(inc)
        not_enough_data_for_feature = 0
        for index, match in inc.iterrows():
            # lf, lfVal = self.calc_league_feature(match['LID'], match['Date'], match['Sea'])
            # hf, hfVal = self.calc_team_feature(match['HID'], match['Date'], match['Sea'], True)
            # af, afVal = self.calc_team_feature(match['AID'], match['Date'], match['Sea'], False)
            pif, pifVal = self.get_pi_features(match['HID'], match['AID'])
            self.update_pi_ratings(match['HID'], match['AID'], match['HSC'] - match['ASC'])
            # mf, mfVal = self.calc_match_importance_features(teamA=match['HID'], teamB=match['AID'], leagueID=match['LID'], date=match['Date'], season=match['Sea'])
            # self.update_league_table(match['HID'], match['AID'], match['LID'], match['Sea'], match['H'], match['D'], match['A'])
            # if not (lfVal and hfVal and afVal and pifVal and mfVal):
            if not pifVal:
                not_enough_data_for_feature += 1
            else:
                feature_vector = list(pif)
                self.history_in_features += [feature_vector]
                self.historical_results += [int(self.get_result_classes_from_onehot(match))]
        assert len(self.historical_results) == len(self.history_in_features),\
            "Historical results (y) and historical features (X) must have same length!"

        print(f'Not enough data for creating feature vector for {not_enough_data_for_feature} matches out of {inc.shape[0]}')

        last_added_date = inc['Date'].iat[-1] if not inc.empty else self.last_trained_date

        if not inc.empty and self.should_train_now(last_inc_date=last_added_date):
            print(f"({time() - self.start_time:7.2f} s) | Training on {last_added_date}")
            self.model.fit(self.history_in_features, self.historical_results)
            self.last_trained_date = last_added_date
            # self.stats.print()

        # probabilities = gbt.predict(opps -> feature_vector)
        if len(opps) > 0:
            matches_to_predict, validity_of_fv = self.get_feature_vectors(opps[['Sea', 'Date', 'LID', 'HID', 'AID']])
            prediction_probs = self.model.predict_proba(matches_to_predict)
            # print(f'prediction probs {prediction_probs}')
            bm_probs = 1 / np.array(opps[['OddsH', 'OddsD', 'OddsA']])
            bets = self.bet(prediction_probs, bm_probs, resources=summary['Bankroll'].item(),
                            min_bet=summary.iloc[0].to_dict()['Min_bet'],
                            max_bet=summary.iloc[0].to_dict()['Max_bet'], strategy=1)
            # self.stats.update(opps, prediction_probs, bm_probs, bets, inc, np.array(opps[['OddsH', 'OddsD', 'OddsA']]))

            return pd.DataFrame(data=bets, columns=['BetH', 'BetD', 'BetA'], index=opps.index)


    def update_team_history(self, teamID, matches, where):
        history_label = f'{where}_HISTORY'
        if history_label not in self.team_features[teamID].keys():
            self.team_features[teamID][history_label] = matches
        else:
            self.team_features[teamID][history_label] = self.team_features[teamID][history_label].append(matches)

    def get_result_classes_from_onehot(self, onehot_df):
        """
        Takes a dataframe with [H, D, A] columns, (which are onehot encoding of results) and returns them as classes,
        with H=0, D=1, A=2
        :param onehot_df:
        :return:
        """
        return (onehot_df[['H', 'D', 'A']].to_numpy().reshape(-1, 3) * np.array([0, 1, 2])).sum(axis=1)

    def get_feature_vectors(self, IDs_df: pd.DataFrame):
        """
        Dataframe with columns [Date, LID, HID, AID]
        :param IDs_df:
        :return:
        """
        #TODO: Do we need Date column? We don't know!
        n_matches = len(IDs_df.index)
        n_features = self.n_pi_features

        features = np.empty((n_matches, n_features), dtype=float)
        validity = np.empty((n_matches), dtype=bool)
        for i_row in range(n_matches):
            date = IDs_df['Date'].iat[i_row]
            season = IDs_df['Sea'].iat[i_row]
            hid = IDs_df['HID'].iat[i_row]
            aid = IDs_df['AID'].iat[i_row]
            lid = IDs_df['LID'].iat[i_row]
            # features[i_row, :self.n_team_features], hfVal = self.calc_team_feature(hid, date, season, True)
            # features[i_row, self.n_team_features:2*self.n_team_features], afVal = self.calc_team_feature(aid, date, season, False)
            # features[i_row, 2*self.n_team_features:2*self.n_team_features+self.n_league_features], lfVal = \
            #     self.calc_league_feature(lid, date, season)
            features[i_row, :], pifVal = self.get_pi_features(hid, aid)
            # features[i_row, 2*self.n_team_features+self.n_league_features+self.n_pi_features:], miVal = \
            #     self.calc_match_importance_features(teamA=hid,
            #                                         teamB=aid,
            #                                         leagueID=lid,
            #                                         date=date,
            #                                         season=season)

            validity[i_row] = pifVal
            # validity[i_row] = hfVal and afVal and lfVal and pifVal and miVal
            # print(features[i_row])
        return features, validity

    def bet(self, prediction_probs, bm_probs, resources, min_bet=5, max_bet=100, strategy=1):
        diff = prediction_probs - bm_probs
        bets = None

        if strategy == 0:  # aggressive
            THRESHOLD = 0.01
            diff[diff <= THRESHOLD] = 0
            if np.count_nonzero(diff[diff > THRESHOLD]) == 0:
                diff[:, :] = 0
                return diff
            bet = min(max_bet, math.floor(resources / np.count_nonzero(diff[diff > THRESHOLD])))
            diff[diff > THRESHOLD] = bet
            bets = diff
        elif strategy == 1:  # less aggressive
            THRESHOLD = 0.02
            diff[diff <= THRESHOLD] = 0
            if np.count_nonzero(diff[diff > THRESHOLD]) == 0:
                diff[:, :] = 0
                bets = diff
            else:
                max_bet_sum = resources // 15

                bet = min(max_bet, math.floor(max_bet_sum / np.count_nonzero(diff[diff > THRESHOLD])))
                if bet < min_bet:
                    bet = min_bet
                    available = max_bet_sum
                    for i in range(len(prediction_probs)):
                        for j in range(3):
                            if diff[i, j] > 0:
                                diff[i, j] = bet
                                available -= bet
                            if available < bet:
                                bet = 0
                    bets = diff
                else:
                    diff[diff > THRESHOLD] = bet
                    bets = diff
        else:
            print('INVALID BETTING STRATEGY', file=sys.stderr)
            exit(1)

        # # print(f'bets:\n{bets}')
        # print('pred/bm/bet\n-----------------')
        # for i in range(len(prediction_probs)):
        #     for l in (prediction_probs[i, :], bm_probs[i, :], bets[i, :]):
        #         for j in range(3):
        #             print(f'{l[j]:.3f} ', end='')
        #         print()
        #     print('-----------------')

        return bets
