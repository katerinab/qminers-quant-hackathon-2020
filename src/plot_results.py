
import matplotlib.pyplot as plt
import pandas as pd
import math
import os
from environment import INIT_BANKROLL


def plot(PLOT_LATEST=True, SHOW_PLOT=True, SAVE_PLOT=True, fn=None):

    if PLOT_LATEST:
        fn = sorted(list(filter(lambda s: s.endswith('.txt'), os.listdir('../results'))))[-1]

    full_name = f'../results/{fn}'

    with open(full_name, 'r') as f:
        results = list(map(lambda s: s.split(), f.readlines()))
    dates, totals, log_totals = [], [], []
    for date, total in results:
        dates.append(date)
        totals.append(float(total))
        log_totals.append(math.log(float(total)/INIT_BANKROLL, 2))


    df = pd.DataFrame({'date': dates, 'total': totals, 'log(total)': log_totals})

    # log plot
    if SHOW_PLOT:
        print(f'Plotting \'{fn}\'')
        df[['date', 'log(total)']].set_index('date').plot(title=f'{fn} - total {int(totals[-1])}',
                                                          xticks=range(0, len(dates), max(1, len(dates)//19)))
        plt.xticks(rotation=45)
        plt.tight_layout()
        plt.grid()
        plt.show()
    if SAVE_PLOT:
        print(f'Saving plot to \'{fn.split(".")[0]}.png\'')
        df[['date', 'log(total)']].set_index('date').plot(title=f'{fn} - total {int(totals[-1])}',
                                                          xticks=range(0, len(dates), max(1, len(dates)//19)))
        plt.xticks(rotation=45)
        plt.tight_layout()
        plt.grid()
        plt.savefig(f'../results/{fn.split(".")[0]}.png')


if __name__ == '__main__':
    plot(PLOT_LATEST=False, SHOW_PLOT=True, SAVE_PLOT=True, fn='res_saved/003-subsample0.8-ratio1.txt')
