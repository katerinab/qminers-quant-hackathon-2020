import numpy as np

class Stats:
	def __init__(self):
		self.results_seen = []
		self.predictions_made = []

		self.num_correct_predictions = 0

		self.num_skipped_prediction = 0
		self.match_to_pred_res_dir = dict()
		self.trained_on_matches = []


	def get_result_classes_from_onehot(self, onehot_df):
		"""
		Takes a dataframe with [H, D, A] columns, (which are onehot encoding of results) and returns them as classes,
		with H=0, D=1, A=2
		:param onehot_df:
		:return:
		"""
		return (onehot_df[['H', 'D', 'A']].to_numpy().reshape(-1, 3) * np.array([0, 1, 2])).sum(axis=1)

	def update(self, matches_to_predict, prediction_probs, bm_probs, bets, training_data, bm_rate):
		# betting y to matchID that has probability of x to win, real result is z, bookmaker predicted u
		for index, match in training_data.iterrows():
			matchID = index
			self.trained_on_matches += [matchID]
			if matchID in self.match_to_pred_res_dir:
				our_bet = self.match_to_pred_res_dir[matchID][3]
				rate = self.match_to_pred_res_dir[matchID][2]
				real_result = int(self.get_result_classes_from_onehot(match))
				money_gained = 0
				for i in range(3):
					if i == real_result:
						money_gained += (our_bet[i] * rate[i])
					else:
						money_gained -= (our_bet[i] * rate[i])
				self.match_to_pred_res_dir[matchID][-1] = money_gained

		self.results_seen += [training_data.shape[0]]

		num_options = matches_to_predict.shape[0]
		for (index, match), i in zip(matches_to_predict.iterrows(), range(num_options)):
			matchID = index
			self.match_to_pred_res_dir[matchID] = [prediction_probs[i, :], bm_probs[i, :], bm_rate[i, :], bets[i, :], None]

	def print(self):
		prob_diffs = []
		money_gained = []
		money_lost = []
		for matchID, (pred_prob, bm_prob, bm_rate, bets, result) in self.match_to_pred_res_dir.items():
			if result is not None:
				# print(f'{pred_prob} {bm_prob} {bets} {result}')
				prob_diffs += [np.abs(np.array(pred_prob) - np.array(bm_prob))]
				if result > 0:
					money_gained += [result]
				elif result < 0:
					money_lost += [result]
		prob_diffs = np.array(prob_diffs)
		money_lost = np.array(money_lost)
		money_gained = np.array(money_gained)

		if prob_diffs.size > 0:
			print("---- Probabilities diff stats")
			print(f'mean: {prob_diffs.mean(axis=0)}')
			print(f'median: {np.median(prob_diffs, axis=0)}')
			print(f'max: {np.max(prob_diffs, axis=0)}')
			print(f'min: {np.min(prob_diffs, axis=0)}')
			print(f'std: {prob_diffs.std(axis=0)}')
			print("---- Lost money stats")
			print(f'Lost money in {money_lost.shape[0]} cases out of {len(self.match_to_pred_res_dir)}')
			if len(money_lost) > 0:
				print(f'mean: {money_lost.mean(axis=0)}')
				print(f'median: {np.median(money_lost, axis=0)}')
				print(f'max: {np.max(money_lost, axis=0)}')
				print(f'min: {np.min(money_lost, axis=0)}')
				print(f'std: {money_lost.std(axis=0)}')
				print("---- Gained money stats")
				print(f'Gained money in {money_gained.shape[0]} cases out of {len(self.match_to_pred_res_dir)}')
			if len(money_gained) > 0:
				print(f'mean: {money_gained.mean(axis=0)}')
				print(f'median: {np.median(money_gained, axis=0)}')
				print(f'max: {np.max(money_gained, axis=0)}')
				print(f'min: {np.min(money_gained, axis=0)}')
				print(f'std: {money_gained.std(axis=0)}')

		print(f"---------------------- Training data seen {len(self.trained_on_matches)} Predictions made {len(self.match_to_pred_res_dir)}")
		print("\n")